export const FR =
{
  //A
  "accept":"Accepter",
  "add":"Ajout",
  "added.admin":"Ajouté par admin",
  "admin":"Admin",
  "all":"Tous",
  "all.the":"Tous les",
  "anonymous":"Anonyme",
  "anonymous.email":"Anonyme avec email",
  "anonymous.link":"Anonyme avec lien",
  "around":"autour de",
  "around.map.center":"autour du centre de la carte",

  //B
  "back.to.default.view":"Retourner à la vue par défault",
  "back.to.map":"Retour à la carte",
  "by":"Par",

  //C
  "cancel":"Annuler",
  "can.not.locate.address":"Impossible de localiser cette adresse !",
  "card":"Fiche",
  "change.tiles":"Changer le Fond de Carte",
  "check.uncheck.all":"Cocher/Décocher Tout",
  "close":"Fermer",
  "close.menu":"Fermer le Menu",
  "collaborative.moderation":"Modération collaborative",
  "collaborative.moderation.first.paragraph":"Lorsqu'un élément est ajouté ou modifié, la mise à jour des données n'est pas instantanée. L'élément va d'abords apparaître 'grisé sur la carte, et il sera alors possible à tous les utilisateurs logué de voter une et une seule fois pour cet élément. Ce vote n'est pas une opinion, mais un partage de connaissance. Si vous connaissez cet élément, ou savez que cet élément n'existe pas, alors votre savoir nous intéresse !",
  "collaborative.moderation.second.paragraph":"Au bout d'un certain nombre de votes, l'élément pourra alors être automatiquement validé ou refusé. En cas de litige (des votes à la fois positifs et négatifs), un modérateur interviendra au plus vite. On compte sur vous!",
  "comment.for.moderation":"Commentaire pour la modération",
  "copy.html.code":"Copiez ce code Html dans votre site web !",

  //D
  "decide":"Décider",
  "decide.for":"Décidez pour",
  "does.not.exist":"N'existe pas",
  "does.not.respect.charter":"Ne respecte pas la charte",
  "does.not.respect.charter.nothing.to.do.here":"ne respecte pas la charte, il n'a rien à faire ici",
  "duplicate.on.map":"est référencé plusieurs fois sur la carte (doublon)",

  //E
  "edit":"Modifier",
  "element":"élément",
  "element.definite":"l'élément",
  "element.indefinite":"un élément",
  "element.no.longer.exists":"L'élement n'existe plus",
  "element.no.respect.charter":"L'élément ne respecte pas la charte",
  "element.plural":"éléments",
  "element.referenced.several.times":"L'élément est référence plusieurs fois",
  "email.will.be.sent.to":"Un email sera envoyé à ",
  "email.content":"Contenu du message",
  "error.occured":"Désolé, une erreur s'est produite",
  "enter.an.address.postal.code.city":"Entrez une adresse, un CP, une ville...",
  "enter.the.name.of":"Entrez le nom d'",
  "enter.valid.email":"Veuillez renseigner une adresse mail valide",
  "error.occurend.route.calculation":"Une erreur est survenue pendant le calcul de l'itinéraire, désolé !",
  "errors.reported":"Erreurs signalées",
  "exists":"Existe",
  "exists.and.correct.informations":"Existe et les informations sont correctes",
  "exists.but.given.informations.are.incorrect":"existe mais les informations indiquées ne sont pas correctes",
  "exists.but.i.do.not.know.anything.else.about.it":"existe, mais je n'en sais pas plus sur lui",
  "exists.and.i.validate.the.accuracy.of.the.given.informations":"existe et je valide l'exactitude des informations renseignées",
  "exists.but.incorrect.informations":"Existe mais les informations sont incorrectes",
  "export.iframe":"Exporter une Iframe",

  //F
  "files": "Fichiers",
  "fill.fields.below":"Vous devez remplir les champs ci-dessous",
  "find.a.place":"Recherchez un lieu",
  "find.route":"Calculer l'itinéraire",
  "found.one.or.several":"(s) trouvé-e(s)",

  //G
  "general.infos":"Informations générales",
  "geolocalize":"Geolocaliser votre position",
  "geolocation.error":"Erreur de géolocalisation",

  //H
  "height":"Hauteur",
  "historical":"Historique",

  //I
  "i.do.not.agree.with.the.proposed.modifications":"Je ne suis suis pas d'accord avec les modifications proposées",
  "i.validate.the.proposed.modifications":"Je valide les modifications proposées",
  "iframe.initialized.position.map":"L'iframe sera initialisée dans la position actuelle de la carte. Déplacez la carte dans la position voulue avant d'ouvrir cette fenêtre !",
  "import":"Import",
  "informations.incorrect":"Les informations sont incorrectes",

  //L
  "label":"Etiquetter",
  "laozi":"Lao Tseu",
  "laozi.quotation":"« L’échec est le fondement de la réussite. »",
  "list.of":"Liste des",
  "loading":"Chargement...",
  "logged.user":"Utilisateur loggué",

  //M
  "map.of":"Carte des",
  "mark.as.resolved":"Marqué comme résolu",
  "menu":"menu",
  "modification":"Modification",
  "modification.plural":"Modifications",
  "modified.admin":"Modifié par admin",
  "modified.direct.link":"Modifié avec lien direct",
  "modified.owner":"Modifié par propriétaire",
  "modified.pending":"en attente de modification",
  "my.position":"ma position",

  //N
  "new":"Nouvel ",
  "no.category.provided":"Aucune catégorie renseignée",
  "no.longer.exist":"n'existe plus",
  "no.moderation.necessary":"Pas de modération nécessaire",
  "no.result.found":"Aucun résultat trouvé",
  "non.consensual.votes":"Votes non consensuels",
  "not.correct.informations":"Les informations sont incorrectes (si vous êtes en mesure de les corriger, merci de le faire via le bouton 'Proposer des Modifications')",

  //O
  "ok":"OK!",
  "openhours":"Horaires d'ouverture",
  "other.infos":"Autres Informations",

  //P
  "pending.add":"En attente (ajout)",
  "pending.for.too.long":"En attente depuis trop longtemps",
  "pending.modifications":"En attente (modifications)",
  "pending.validation":"En cours de validation",
  "place":"Un lieu",
  "placeholder.input.comment":"Justificatif en cas de refus (qui sera inséré dans l'e-mail automatique)",
  "placeholder.input.comment.administration":"Commentaire optionnel pour la modération",
  "placeholder.input.subject.email":"Objet du message",
  "potential.duplicate":"Doublon potentiel",
  "propose.changes":"Proposer des modifications",
  "proposed.by":"Proposé par",

  //R
  "refused.admin":"Refusé (admin)",
  "refused.votes":"Refusé (votes)",
  "reject":"Refuser",
  "rejected.or.deleted.element":"(Cet élément a été refusé ou supprimé)",
  "remove":"Supprimer",
  "remove.from.favorites":"Retirer des favoris",
  "removed":"Supprimé",
  "report":"Signaler",
  "report.error":"Signaler une erreur",
  "report.error.regarding":"Signaler une erreur concernant",
  "reporting":"Signalement",
  "results":"Résultats",
  "route":"Itinéraire",
  "route.calculation":"Calcul d'itinéraire",
  "route.to.element":"Itinéraire vers cet élément",

  //S
  "save":"Enregistrer",
  "saved":"Enregistré",
  "save.as.favorites":"Enregistrer comme favoris",
  "search":"Chercher",
  "search.for":"Recherche de",
  "see.on.map":"Voir sur la carte",
  "select.type.error":"Vous devez sélectionner un type d'erreur!",
  "send":"Envoyer",
  "send.email":"Envoyer un e-mail",
  "send.email.to":"Envoyer un mail à ",
  "share.link":"Partager ce lien avec d'autres",
  "share.url":"Partager l'URL",
  "share.your.knowledge.about":"Partagez votre connaissance concernant",
  "show.as.list":"Afficher sous forme de liste",
  "show.hide.detail":"Afficher/Masquer le détail",
  "show.more":"Afficher plus",
  "show.on.map":"Afficher sur la carte",
  "show.only":"Afficher uniquement",
  "show.only.actors.with.label":"Afficher uniquement les acteurs avec l'étiquette",
  "show.only.elements.to.moderate":"Afficher uniquement les éléments à modérer",
  "show.only.elements.validation.process":"Afficher uniquement les éléments en cours de validation",
  "show.only.favorites":"Afficher uniquement les favoris",
  "show.only.selected.categories":"Afficher uniquement les catégories actuellement sélectionnées",
  "source":"Origine",
  "starting.address":"L'adresse de départ",
  "starting.address.title":"Entrez une adresse de départ",

  //T
  "the":"le",
  "the.addition":"l'ajout",
  "to.moderate":"A modérer",
  "type.message.here":"Taper votre message ici",

  //U
  "updated.at":"Dernières mise à jour",

  //V
  "validated.admin":"Validé (admin)",
  "validated.votes":"Validé (votes)",
  "video":"Video",
  "vote":"Voter",

  //W
  "waiting":"En attente",
  "what.error.to.report":"Quelle erreur voulez-vous signaler ?",
  "width":"Largeur",

  //Y
  "you.must.select.your.vote":"Vous devez choisir votre vote",
  "your.email":"Votre adresse mail",
  "your.favorites":"Vos favoris"
}